from query_analysis.features.simple_n_grams import SimpleNGrams

def get_top_links(tws, n=20):
    """Returns the links most shared in the data set retrieved in
       the order of how many times each was shared."""
    freq = SimpleNGrams(char_upper_cutoff=100, tokenizer="space")
    for x in tws:
        link_str = '  '.join([lk['expanded_url'] for lk in x.entities['urls'] if lk])

        if link_str != "None" and len(link_str) > 3:
            freq.add(link_str)
        else:
            freq.add("NoLinks")
    return freq.get_tokens(n)


def get_top_grams(tws, n=20):
    freq = SimpleNGrams(char_upper_cutoff=20, tokenizer="twitter")
    freq.sl.add_session_stop_list(["http", "https", "amp", "htt"])
    for x in tws:
        freq.add(x.text)
    return freq.get_tokens(n)


def get_top_hastags(tws, n=20):
    """Returns the links most shared in the data set retrieved in
       the order of how many times each was shared."""
    freq = SimpleNGrams(char_upper_cutoff=100, tokenizer="space")
    for x in tws:
        ht_str = '  '.join([lk['text'].lower() for lk in x.entities['hashtags'] if lk])
        if ht_str != "None" and len(ht_str) > 3:
            freq.add(ht_str)
        else:
            freq.add("NoHashtags")
    return freq.get_tokens(n)