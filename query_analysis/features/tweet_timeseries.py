#!/usr/bin/env python
# -*- coding: UTF-8 -*-


import logging
import configparser as ConfigParser
import re
import argparse
import calendar
import os
import datetime
from operator import itemgetter
from query_analysis.data.results import *
from query_analysis.data.tweepy_search import *

import warnings
warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd driver lwork query error")


CHAR_UPPER_CUTOFF = 20          # don't include tokens longer than CHAR_UPPER_CUTOFF
TWEET_SAMPLE = 4000             # tweets to collect for peak topics
MIN_SNR = 2.0                   # signal to noise threshold for peak detection
MAX_N_PEAKS = 7                 # maximum number of peaks to output
MAX_PEAK_WIDTH = 20             # max peak width in periods
MIN_PEAK_WIDTH = 1              # min peak width in periods
SEARCH_PEAK_WIDTH = 3           # min peak width in periods
N_MOVING = 4                    # average over buckets

# basic defaults
FROM_PICKLE = False
DEFAULT_CONFIG_FILENAME = os.path.join(".",".gnip")
DATE_FMT = "%Y%m%d%H%M"
DATE_FMT2 = "%Y-%m-%dT%H:%M:%S"
LOG_FILE_PATH = os.path.join(".","time_series.log")


# set up simple logging
logging.basicConfig(filename=LOG_FILE_PATH,level=logging.DEBUG)
logging.info("#"*70)
logging.info("################# started {} #################".format(datetime.datetime.now()))


logging.debug("CHAR_UPPER_CUTOFF={},TWEET_SAMPLE={},MIN_SNR={},MAX_N_PEAKS={},MAX_PEAK_WIDTH={},MIN_PEAK_WIDTH={},SEARCH_PEAK_WIDTH={},N_MOVING={}".format(
    CHAR_UPPER_CUTOFF
    , TWEET_SAMPLE
    , MIN_SNR
    , MAX_N_PEAKS
    , MAX_PEAK_WIDTH
    , MIN_PEAK_WIDTH
    , SEARCH_PEAK_WIDTH
    , N_MOVING))

class TimeSeries():
    """Containter class for data collected from the API and associated analysis outputs"""
    pass


class TwitterSearchTimeseries():

    def __init__(self, query, lang, start_date, end_date, token_list_size=40):
        """Retrieve and analysis timesseries and associated interesting trends, spikes and tweet content."""
        # default tokenizer and character limit
        char_upper_cutoff = CHAR_UPPER_CUTOFF
        self.token_list_size = int(token_list_size)
        #############################################
        # CONFIG FILE/COMMAND LINE OPTIONS PATTERN
        # parse config file
        config_from_file = self.config_file()
        # set required fields to None.  Sequence of setting is:
        #  (1) config file
        #  (2) command line
        # if still none, then fail
        self.user = None
        self.password = None
        self.stream_url = None
        if config_from_file is not None:
            try:
                # command line options take presidence if they exist
                self.user = config_from_file.get('creds', 'un')
                self.password = config_from_file.get('creds', 'pwd')
                self.stream_url = config_from_file.get('endpoint', 'url')
            except (ConfigParser.NoOptionError,
                    ConfigParser.NoSectionError) as e:
                logging.warn("Error reading configuration file ({}), ignoring configuration file.".format(e))
        # parse the command line options
        self.options = self.args().parse_args()
        self.options.start = start_date
        self.options.end = end_date
        self.options.filter = query
        self.options.count_bucket = 'day'
        self.options.get_topics = True
        self.options.lang = lang
        # decode step should not be included for python 3
        if sys.version_info[0] == 2:
            self.options.filter = self.options.filter.decode("utf-8")
            if self.options.second_filter:
                self.options.second_filter = self.options.second_filter.decode("utf-8")
        # set up the job
        # over ride config file with command line args if present
        if self.options.user is not None:
            self.user = self.options.user
        if self.options.password is not None:
            self.password = self.options.password
        if self.options.stream_url is not None:
            self.stream_url = self.options.stream_url

        # search v2 uses a different url
        if "gnip-api.twitter.com" not in self.stream_url:
            logging.error("gnipSearch timeline tools require Search V2. Exiting.")
            logging.error(
                "Your URL should look like: https://gnip-api.twitter.com/search/fullarchive/accounts/<account>/dev.json")
            sys.stderr.write("gnipSearch timeline tools require Search V2. Exiting.\n")
            sys.stderr.write(
                "Your URL should look like: https://gnip-api.twitter.com/search/fullarchive/accounts/<account>/dev.json")
            sys.exit(-1)

        # set some options that should not be changed for this anaysis
        self.options.paged = True
        self.options.search_v2 = True
        self.options.max = 500
        self.options.query = False


        # log the attributes of this class including all of the options
        for v in dir(self):
            # except don't log the password!
            if not v.startswith('__') and not callable(getattr(self, v)) and not v.lower().startswith('password'):
                tmp = str(getattr(self, v))
                tmp = re.sub("password=.*,", "password=XXXXXXX,", tmp)
                logging.debug("  {}={}".format(v, tmp))

    def config_file(self):
        """Search for a valid config file in the standard locations."""
        config = ConfigParser.ConfigParser()
        # (1) default file name precidence

        config.read(DEFAULT_CONFIG_FILENAME)
        logging.info("attempting to read config file {}".format(DEFAULT_CONFIG_FILENAME))
        if not config.has_section("creds"):
            # (2) environment variable file name second
            if 'GNIP_CONFIG_FILE' in os.environ:
                config_filename = os.environ['GNIP_CONFIG_FILE']
                logging.info("attempting to read config file {}".format(config_filename))
                config.read(config_filename)
        if config.has_section("creds") and config.has_section("endpoint"):
            return config
        else:
            logging.warn("no creds or endpoint section found in config file, attempting to proceed without config info from file")
            return None

    def args(self):
        "Set up the command line argments and the associated help strings."""
        twitter_parser = argparse.ArgumentParser(
                description="GnipSearch timeline tools")
        twitter_parser.add_argument("-b", "--bucket", dest="count_bucket",
                default="day",
                help="Bucket size for counts query. Options are day, hour, minute (default is 'day').")
        twitter_parser.add_argument("-e", "--end-date", dest="end",
                default=None,
                help="End of datetime window, format 'YYYY-mm-DDTHH:MM' (default: most recent activities)")
        twitter_parser.add_argument("-f", "--filter", dest="filter",
                default="from:jrmontag OR from:gnip",
                help="PowerTrack filter rule (See: http://support.gnip.com/customer/portal/articles/901152-powertrack-operators)")
        twitter_parser.add_argument("-g", "--second_filter", dest="second_filter",
                default=None,
                help="Use a second filter to show correlation plots of -f timeline vs -g timeline.")
        twitter_parser.add_argument("-l", "--stream-url", dest="stream_url",
                default=None,
                help="Url of search endpoint. (See your Gnip console.)")
        twitter_parser.add_argument("-p", "--password", dest="password", default=None,
                help="Password")
        twitter_parser.add_argument("-s", "--start-date", dest="start",
                default=None,
                help="Start of datetime window, format 'YYYY-mm-DDTHH:MM' (default: 30 days ago)")
        twitter_parser.add_argument("-u", "--user-name", dest="user",
                default=None,
                help="User name")
        twitter_parser.add_argument("-t", "--get-topics", dest="get_topics", action="store_true",
                default=False,
                help="Set flag to evaluate peak topics (this may take a few minutes)")
        twitter_parser.add_argument("-w", "--output-file-path", dest="output_file_path",
                default=None,
                help="Create files in ./OUTPUT-FILE-PATH. This path must exists and will not be created. This options is available only with -a option. Default is no output files.")
        return twitter_parser

    def get_timeline(self):
        """Execute API calls to the timeseries data and popular content tweet data we need for analysis. Perform analysis
        as we go because we often need results for next steps."""
        ######################
        # (1) Get the timeline
        ######################
        logging.info("retrieving timeline counts")
        results_timeseries = Results(self.user
                                     , self.password
                                     , self.stream_url
                                     , self.options.paged
                                     , self.options.output_file_path
                                     , pt_filter=self.options.filter + ' (lang:'+self.options.lang+')'
                                     , max_results=int(self.options.max)
                                     , start=self.options.start
                                     , end=self.options.end
                                     , count_bucket=self.options.count_bucket
                                     , show_query=self.options.query
                                     )
        # sort by date
        res_timeseries = sorted(results_timeseries.get_time_series(), key=itemgetter(0))
        # if we only have one activity, probably don't do all of this
        if len(res_timeseries) <= 1:
            raise ValueError("You've only pulled {} Tweets. time series analysis isn't what you want.".format(
                len(res_timeseries)))
        # calculate total time interval span
        time_min_date = min(res_timeseries, key=itemgetter(2))[2]
        time_max_date = max(res_timeseries, key=itemgetter(2))[2]
        time_min = float(calendar.timegm(time_min_date.timetuple()))
        time_max = float(calendar.timegm(time_max_date.timetuple()))
        time_span = time_max - time_min
        logging.debug("time_min = {}, time_max = {}, time_span = {}".format(time_min, time_max, time_span))
        # create a simple object to hold our data
        ts = TimeSeries()
        ts.volume = 0
        ts.dates = []
        ts.x = []
        ts.counts = []
        # load and format data
        for i in res_timeseries:
            ts.dates.append(i[2])
            ts.counts.append(float(i[1]))
            ts.volume = ts.volume + float(i[1])
            # create a independent variable in interval [0.0,1.0]
            ts.x.append(
                (calendar.timegm(datetime.datetime.strptime(i[0], DATE_FMT).timetuple()) - time_min) / time_span)
        logging.info("read {} time items from search API".format(len(ts.dates)))

        ts.topics = []
        logging.info("retrieving tweets for topics")
        print("retrieving tweets for topics", self.options.filter, self.options.lang)
        res = TweepySearch(self.options.filter, self.options.lang)
        res.get_sample_tweets(result_type='popular')
        logging.info("retrieved {} records".format(len(res.tws)))
        #n_grams_counts = list(res.get_top_grams(n=self.token_list_size))
        #ts.topics.append(n_grams_counts)
        ts.res = res
        return ts