import matplotlib.pyplot as plt
import datetime
import logging
import string
import pandas as pd


PLOT_DELTA_Y = 1.2              # spacing of y values in dotplot
# set up simple logging
logging.info("################# started {} #################".format(datetime.datetime.now()))


def plot_timelines(q_ts_tuplist):
    """Basic choice for plotting analysis, takes tuple of query and timeseries Returned by features"""
    def get_legendname(query):
        valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
        filter_prefix_name = ''.join(c for c in query if c in valid_chars)
        filter_prefix_name = filter_prefix_name.replace(" ", "_")
        if len(filter_prefix_name) > 16:
            filter_prefix_name = filter_prefix_name[:16]
        return filter_prefix_name


    ######################
    # timeline
    ######################
    #fig, ((plt1, plt2), (plt3, plt4)) = plt.subplots(2, 2)
    # plt1 = plt.subplot(2, 2, 1)
    fig, ax = plt.subplots()
    legends = []
    for q, ts in q_ts_tuplist:
        legends.append(get_legendname(q))
        df0 = pd.Series(ts.counts, index=ts.dates)
        df0.plot(ax=ax)
    ax.set_ylabel("Counts")
    lines, _ = ax.get_legend_handles_labels()
    ax.legend(lines, legends, loc='best')
    return ax


def dotplot(x, labels):
    """Makeshift dotplots in matplotlib. This is not completely general and encodes labels and
    parameter selections that are particular to n-gram dotplots."""
    #logging.info("dotplot called, writing image to path={}".format(path))
    if len(x) <= 1 or len(labels) <= 1:
        raise ValueError("cannot make a dot plot with only 1 point")
    # split n_gram_counts into 2 data sets
    n = int(len(labels)/2)
    x1, x2 = x[:n], x[n:]
    labels1, labels2 = labels[:n], labels[n:]
    # create enough equally spaced y values for the horizontal lines
    ys = [r*PLOT_DELTA_Y for r in range(1,len(labels2)+1)]
    # give ourselves a little extra room on the plot
    maxx = max(x)*1.05
    maxy = max(ys)*1.05
    # set up plots to be a factor taller than the default size
    # make factor proportional to the number of n-grams plotted
    size = plt.gcf().get_size_inches()
    # factor of n/10 is empirical
    scale_denom = 10
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1,figsize=(size[0], size[1]*n/scale_denom))
    logging.debug("plotting top {} terms".format(n))
    logging.debug("plot size=({},{})".format(size[0], size[1]*n/scale_denom))
    #  first plot 1-grams
    ax1.set_xlim(0,maxx)
    ax1.set_ylim(0,maxy)
    ticks = ax1.yaxis.set_ticks(ys)
    text = ax1.yaxis.set_ticklabels(labels1)
    for ct, item in enumerate(labels1):
        ax1.hlines(ys[ct], 0, maxx, linestyle='dashed', color='0.9')
    ax1.plot(x1, ys, 'ko')
    ax1.set_title("1-grams")
    # second plot 2-grams
    ax2.set_xlim(0,maxx)
    ax2.set_ylim(0,maxy)
    ticks = ax2.yaxis.set_ticks(ys)
    text = ax2.yaxis.set_ticklabels(labels2)
    for ct, item in enumerate(labels2):
        ax2.hlines(ys[ct], 0, maxx, linestyle='dashed', color='0.9')
    ax2.plot(x2, ys, 'ko')
    ax2.set_title("2-grams")
    ax2.set_xlabel("Fraction of Mentions")
    #
    plt.tight_layout()
    return plt

def plots_text(ts):
    ######################
    # n-grams to help determine topics of peaks
    ######################
    plt = None
    for n, p in enumerate(ts.topics):
        x = []
        labels = []
        for i in p:
            x.append(i[1])
            labels.append(i[4])
        try:
            logging.info("creating n-grams dotplot for peak {}".format(n))
            plt = dotplot(x, labels)
        except ValueError as e:
            logging.error("{} - plot  skipped".format(e))
    return plt
