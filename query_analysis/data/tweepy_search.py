#!/usr/bin/env python
# -*- coding: UTF-8 -*-


from __future__ import unicode_literals
import tweepy
import time


class TweepySearch:
    '''
    Collect data for query of last one week provided by public twitter api
    '''
    consumer_key = "oodb2tOQybYuwoyQ7CRmQ"
    consumer_secret = "C5stpgQ6KjFdYdhEk0rTxWenYB6zvlq6KXvZdeS3eo"
    access_key = "18764138-yu6h0wQMHPbmHjckLPhaFDA4cgXCBxFnjAlCo8PpQ"
    access_secret = "GBOPGy1o32WbC4u9wzG9syeOh95pDA4FcNgy4J7lTL9Nk"

    def __init__(self, query, lang):
        self.query = query
        self.lang = lang
        auth = tweepy.OAuthHandler(TweepySearch.consumer_key, TweepySearch.consumer_secret)
        auth.set_access_token(TweepySearch.access_key, TweepySearch.access_secret)
        self.api = tweepy.API(auth)
        # TODO: add support for multiple apis

    def get_sample_tweets(self, result_type='mixed', cnt=1000):
        '''
        Get tweets for a initial query
        :param result_type: mixed or popular
        :param cnt: Number of Tweets retrieved
        :return:
        '''

        def limit_handled(cursor):
            while True:
                try:
                    yield cursor.next()
                except tweepy.TweepError as e:
                    print(e)
                    print('rate limit error -- sleeping for 5 mins')
                    time.sleep(5 * 60)

        tws = []
        for status in limit_handled(
                tweepy.Cursor(self.api.search, q=self.query, result_type='popular', lang=self.lang).items(cnt)):
            tws.append(status)
        if result_type == 'mixed':
            for status in limit_handled(
                    tweepy.Cursor(self.api.search, q=self.query, result_type='recent', lang=self.lang).items(cnt)):
                tws.append(status)

        self.tws = tws
        return tws
